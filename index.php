<!DOCTYPE html>
/* include 'calculate.php'; */
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BMI Calculator</title>
    <meta name="description" content="Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/add.css" rel="stylesheet"> <!-- You could add your own css-definitions in a file - but you have to create it first... -->
    <!-- The following css-definitions are used just for showing you where the components of this page are placed. Feel free to delete the whole style-tag and to remove the classes in the html elements. -->
    <style>
        .bg-color01 {
            background-color: #AAA;
        }
        .bg-color02 {
            background-color: #9BC;
        }
        .bg-color03 {
            background-color: #579;
        }
        div[class*='level'] {
            border: 1px solid black;
            margin: 2px;
        }
    </style>
    <!--[if lt IE 9]>
      <script src="./js/html5shiv.min.js"></script>
      <script src="./js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header class="container">
        <p>The header is usually the place for a logo or a picture, a navigation bar or a search field.</p>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Solve IT yourself!</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Rechner <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="polish-vet.php">Polish VET at Zespół Szkół Nr 2 w Milanówku</a></li>
                                <li><a href="german-vet.php">German VET at Eckener-Schule Flensburg</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="overview.php">Project-overview</a></li>
                            </ul>
                        </li>
                        <li><a href="location.php">Find us</a></li>
                        <li><a href="contact.php"><span class="glyphicon glyphicon-envelope"></span>  Contact us</a></li>
                        <li><a href="policies-and-legislation.php">Policies and legislation</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <section class="container bg-color01">
        <h1>Hi, developers!</h1>
        <p>Here comes the content..</p>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-1">
				</div>
				<!-- Container with form for BMI Calculator-->
				<div class="col-md-10">
					<form method= "get">
					  <div class="form-group row">
						<label for="age" class="col-4 col-form-label">Alter</label> 
						<div class="col-8">
						  <input id="age" name="age" placeholder="bitte das Alter in vollen Jahren angeben" type="number" min = "18" max = "99" class="form-control">
						</div>
					  </div>
					  <div class="form-group row">
						<label for="weight" class="col-4 col-form-label">Gewicht</label> 
						<div class="col-8">
						  <input id="weight" name="weight" placeholder="bitte das Gewicht in vollen KG angeben" type="number" min = "35" max = "300" required="required" class="form-control">
						</div>
					  </div>
					  <div class="form-group row">
						<label for="height" class="col-4 col-form-label">Größe</label> 
						<div class="col-8">
						  <input id="height" name="height" placeholder="bitte die Größe in Zentimeter angeben" type="number" min = "100" max = "250" class="form-control" required="required">
						</div>
					  </div>
					  <div class="form-group row">
						<label for="gender" class="col-4 col-form-label">Geschlecht</label> 
						<div class="col-8">
						  <select id="gender" name="gender" aria-describedby="genderHelpBlock" class="custom-select" required="required">
							<option value="male">Männlich</option>
							<option value="female">Weiblich</option>
						  </select> 
						  <span id="genderHelpBlock" class="form-text text-muted">Bitte wählen Sie ihr Geschlecht</span>
						</div>
					  </div> 
					  <div class="form-group row">
						<div class="offset-4 col-8">
						  <button name="submit" type="submit" class="btn btn-secondary">Berechnen</button>
						</div>
					  </div>
					</form>
				</div>
				<div class="col-md-1">
				</div>
			</div>
		</div>
	</div>
</div>
        <article id="tree"> <!-- this article is filled with example-data -->
            <div class="row level1">
                <div id="content" class="col-md-12">
				<!-- BMI Calculator part-->
<?php
//	if (isset($_GET["submit"])) {
		$weight=filter_input(INPUT_GET, 'weight');
		$height=filter_input(INPUT_GET, 'height');
		//$age=    $_POST["age"];
		$gender=filter_input(INPUT_GET, 'gender');
		$mantabelle= "<br/><br/>Untergewicht = Alles unter 19 <br/>Normalgewicht = 19 - 24 <br/>Übergewicht = 24 - 30 <br/>Adipositas = 30 - 40 <br/> massiv Adipositas = Alles über 40";
		$weibtabelle= "<br/><br/>Untergewicht = Alles unter 20 <br/>Normalgewicht = 20 - 25 <br/>Übergewicht = 25 - 30 <br/>Adipositas = 30 - 40 <br/> massiv Adipositas = Alles über 40";

		//calculation
		$geteilt=$height/100;
		$bmi= $weight/(($geteilt)^2);
		echo "Ihr BMI liegt bei: " . round($bmi) . "<br/>";
		

			if ($gender == "male") { //Männlich
		if ($bmi < 19) {
			echo "Untergewicht";
			echo $mantabelle;
		}
		elseif ($bmi <= 24) {
			echo "Normalgewicht";
			echo $mantabelle;
		}
		elseif ($bmi <= 30) {
			echo "Übergewicht";
			echo $mantabelle;
		}
		elseif ($bmi <= 40) {
			echo "Adipositas";
			echo $mantabelle;
		}
		elseif ($bmi >= 40) {
			echo "massiv Adipositas";
			echo $mantabelle;
		}}
	 if ($gender == "female") { //Weiblich
		if ($bmi <= 20) {
			echo "Untergewicht";
			echo $weibtabelle;
		}
		elseif ($bmi <= 25) {
			echo "Normalgewicht";
			echo $weibtabelle;
		}
		elseif ($bmi <= 30) {
			echo "Übergewicht";
			echo $weibtabelle;
		}
		elseif ($bmi <= 40) {
			echo "Adipositas";
			echo $weibtabelle;
		}
		elseif ($bmi > 40) {
			echo "massiv Adipositas";
			echo $weibtabelle;
					}
				}
		//	} 
?>
                </div>
            </div>
        </article>
        
    </section>
    <footer class="container bg-color02">
        <p>And here is some text in the footer. And always make sure your html is valid here: <br /><a class="btn btn-link btn-sm active" role="button" href="https://validator.w3.org/#validate_by_input" target="_blank">https://validator.w3.org/#validate_by_input</a></p>
    </footer>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.js"></script>
    <script src="./js/script.js"></script> <!-- Place your javascript here... -->
</body>
</html>


